package analysis;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
@Category(TestCase.class)
public class TestShapeClassifierTManual {

    private ShapeClassifier classifier;
    private String string;
    private String expected;

    public TestShapeClassifierTManual(String string, String expected) {
        this.classifier = new ShapeClassifier();
        this.string = string;
        this.expected = expected;
    }

    @Test
    public void test() {
        String result = classifier.evaluateGuess(string);
        assertEquals(expected, result);
    }

    @Parameters
    public static Collection<Object[]> data() {
        Collection<Object[]> ar = new ArrayList();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("testcase_manual.txt"));
            String string;

            while ((string = bufferedReader.readLine()) != null) {
                String clearString = string.replaceAll(" ", "");
                String[] parsed = clearString.split("-");

                Object [] object = new Object[] {parsed[0], parsed[1]};
                ar.add(object);
            }
        } catch (Exception e) {

        }

        return ar;
    }
}
