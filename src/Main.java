import analysis.ShapeClassifier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

public class Main {
    public static void main(String[] args) {
        ShapeClassifier cl = new ShapeClassifier();
        String ss = "Line,Small,No,11";
        ss = ss.replaceAll(" ", "");
        System.out.println(cl.evaluateGuess(ss));
        // runTest("testcase_single.csv", "result_single.txt");
        // runTest("testcase_pairs.csv", "result_pairs.txt");
        // runTest("testcase_triples.csv", "result_triples.txt");
        // runTest("testcase_manual.csv", "result_manual.txt");
    }

    private static void runTest(String fileName, String fileOutput) {
        String finalResult = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String string;

            while ((string = bufferedReader.readLine()) != null) {
                ShapeClassifier classifier = new ShapeClassifier();
                String clearString = string.replaceAll(" ", "");
                String result = classifier.evaluateGuess(clearString);
                finalResult += result + "\n";
            }
        } catch (Exception e) {
            System.out.println("ERROR");
        }
        writeResult(finalResult, fileOutput);
    }

    private static void writeResult(String result, String fileOutput) {
        try {
            FileWriter myWriter = new FileWriter(fileOutput);

            myWriter.write(result);
            myWriter.close();
        } catch (Exception e) {
            System.out.println("Write ERROR");
        }
    }
}
